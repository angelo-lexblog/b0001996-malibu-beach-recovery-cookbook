<?php
/**
 * The template for displaying the footer.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */
?>

</div><!-- .main -->

<?php get_sidebar(); ?>

<?php /*
<!-- sometimes you need a clear div right here <div class="clear_div">.</div> -->
*/ ?>

</div><!-- .mainwrapper -->
</div><!-- close wrapper div started in header.php -->

	<footer id="blog-footer" class="blog-footer">
			<?php if ( is_active_sidebar( 'colophon' ) ) : ?>
			<section class="colophon" role="complementary">
				<?php dynamic_sidebar( 'colophon' ); ?>
			</section>
		<?php endif; ?>	
		
		<div id="blog-footer-inner-wrapper" class="blog-footer-inner-wrapper">
		
			<?php if ( is_active_sidebar( 'footer-contact' ) ) : ?>
				<section class="footer-contact" role="complementary">
					<?php dynamic_sidebar( 'footer-contact' ); ?>
				</section>
			<?php endif; ?>

		
			<?php if ( is_active_sidebar( 'footer-extras' ) ) : ?>
				<section class="footer-extras" role="complementary">
					<?php dynamic_sidebar( 'footer-extras' ); ?>
				</section>
			<?php endif; ?>	
		
		</div>
	

	
		<img class="lexblog-print" src="<?php bloginfo( 'template_directory' ); ?>/images/lexblog.png" alt="Lexblog" />

	</footer>
	


<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
	
</body>
</html>
